<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="beanHello" scope="session" class="fr.ulco.modele.HelloBean">
</jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Titre</title>
</head>
<body>
Bonjour <jsp:getProperty  name="beanHello" property="name"/>
</body>
</html>