<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.Collection, java.util.Iterator,fr.ulco.modele.HelloBean"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Titre</title>
</head>
<body>
<c:forEach var="aBean" items="${beanHello}">
Test ${aBean.name}
</c:forEach>
<!-- 
<%
	Collection<HelloBean> beanHello = (Collection<HelloBean>)
	session.getAttribute("beanHello");
	if (beanHello != null){
		Iterator<HelloBean> it = beanHello.iterator();
		while(it.hasNext())
		{
			HelloBean aBean = it.next();
		%>
			Bonjour <%= aBean.getName() %>
		<%
		}
	}
	%>
	 -->
</body>
</html>