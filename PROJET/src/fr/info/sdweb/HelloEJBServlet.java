package fr.info.sdweb;

import java.io.IOException;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ulco.ejb.HelloEJBRemote;
import fr.ulco.modele.HelloBean;


@WebServlet("/HelloEJB")
public class HelloEJBServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloEJBServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
    ServletException, IOException {
    HttpSession session = request.getSession(true);
    String name = request.getParameter("nom");
    String message = "";
   
    try{
    final Hashtable jndiProperties = new Hashtable();
    jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
    final Context context = new InitialContext(jndiProperties);
    final String appName = "HelloEAR";
    final String moduleName = "HelloEJBProject";
    final String beanName = "HelloJNDI";
    final String viewClassName = HelloEJBRemote.class.getName();
    
    HelloEJBRemote remote = (HelloEJBRemote) context.lookup("ejb:"+appName+"/"+moduleName+"/"+    beanName+"!"+viewClassName);
    message = remote.direBonjour(name);
    
    }catch (Exception e) {
    	e.printStackTrace();
    }
    HelloBean bean = new HelloBean();
    bean.setName(message);
    session.setAttribute("beanHello", bean);
    response.sendRedirect("HelloEJB.jsp");
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
