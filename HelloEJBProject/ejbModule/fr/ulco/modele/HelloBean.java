package fr.ulco.modele;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table (schema="zharrat", name="HelloTable")
public class HelloBean implements Serializable {
	private	int	id	;
	private String aName = "";
	@Id
	@GeneratedValue
	@Column(name="pk_hello")
	public int getId(){
	return id;
	}
	public void setId(int id){
	this.id = id;
	}
	@Column(name="message")
	public String getName(){
	return aName;
	}
	public void setName(String pName){
	aName = pName;
	}
}
