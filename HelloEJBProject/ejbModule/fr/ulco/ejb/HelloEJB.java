package fr.ulco.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import fr.ulco.modele.HelloBean;


/**
 * Session Bean implementation class HelloEJB
 */
@Stateless(name="HelloJNDI")
public class HelloEJB implements HelloEJBRemote, HelloEJBLocal {

	@PersistenceContext(unitName="managerHello")
	EntityManager mh;
	/**
	* Default constructor.
	*/
	public HelloEJB() {
	// TODO Auto-generated constructor stub
	}
	public String direBonjour(String name){
	System.out.println("EJB : preparation du message pour " + name);
	HelloBean bean = new HelloBean();
	bean.setName(name);
	insertObj(bean);
	return "Bonjour " + name;
	}
	
	public void insertObj(HelloBean obj) {
		System.out.println("EJB : Ajout fait ! "+obj);
		mh.persist(obj);
	}
	
	public void updateObj(HelloBean obj) {
		System.out.println("EJB : Updated ! "+obj);
		mh.merge(obj);
	}
	
	public void removeObj(HelloBean obj) {
		System.out.println("EJB : Remove faite ! "+obj);
		mh.remove(obj);
	}
		
	public HelloBean direBonjourEntity(String name){
	Query q = mh.createQuery("select h from HelloBean h");
	HelloBean helloBean = (HelloBean)q.getSingleResult();
	return helloBean;
	}
	public String direBonjourEntity2(String name){
	Query q = mh.createQuery("select h.name from HelloBean h where h.id=:id").setParameter("id", 1);
	String helloBean = (String)q.getSingleResult();
	return helloBean;
	}
}
