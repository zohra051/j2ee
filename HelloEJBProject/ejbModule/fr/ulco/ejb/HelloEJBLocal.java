package fr.ulco.ejb;

import javax.ejb.Local;

@Local
public interface HelloEJBLocal {
	public String direBonjour(String name);
}